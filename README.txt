*************************************************
*            Projet TI - Groupe N°3             *
*             Transformée De Hough              *
*               Année 2021-2022                 *
*           Master 1 - Inforamtique             *
*            UFR STS - 21 000 Dijon             *
*************************************************

+-----------------------------------------------+
|       Enseignante responsable du module       |
+-----------------------------------------------+
|               Céline    ROUDET                |
+-----------------------------------------------+

+-----------------------------------------------+
|       Composition du groupe (6 membres)       |
+-----------------------------------------------+
|               Wassim   DJELLAT                |
|               Dinaël    INGETA                |
|               Achraf   L'HAZLI                |
|               Melissa MAKHLOUF                |
|               Emilien PRATTICO                |
|               Axel   TOUSSENEL                |
+-----------------------------------------------+

=================================================
¤       Liste des commandes importantes         ¤
¤       pour la soutenance (démo projet)        ¤
=================================================
A partir d'une machine fonctionnant sous Linux :

- télécharger et extraire l'archive du projet

- se déplacer dans le répertoire du projet
> cd cheminAbsoluMenantAuRepertoireDuProjet/

- lancer le logiciel MATLAB depuis un terminal
> matlab

- exécuter le script principal dans MATLAB
> hough_main;

- pour obtenir des explications sur une commande
> help nom_de_la_commande

=================================================
¤       Documentation simple BUILD AND RUN      ¤
=================================================
- pour compiler puis exécuter le programme
> matlab -r 'try hough_main; catch; end;'

- idem avec des options spécifiques puis 'quit'
> matlab -nodisplay -nosplash -nodesktop -r 
  'try hough_main; catch; end;'
