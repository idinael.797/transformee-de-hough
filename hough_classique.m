% fonction de détection de droites qui prend en paramètre une image I et
% retourne une image res (avec les droites détectées mises en évidence)
function res = hough_classique(I)

    % récupération du nombre de ligne puis de colonnes de l'image I
    [nbL,nbC, can] = size(I);    % OU... nbL = size(I, 1); nbC = size(I, 2);
    if can > 1
        I = rgb2gray(I);
        I = edge(I, "canny");
    end

    % calcul des valeurs minimum et maximum de rhô
    rhoMin = -sqrt(double(nbL-1)^2+double(nbC-1)^2);
    rhoMax = sqrt(double(nbL-1)^2+double(nbC-1)^2);

    % affectation des valeurs minimum et maximum de theta
    thetaMin = 0;
    thetaMax = 180;

    % affectation du pas d'échantillonnage delta pour rhô et theta
    deltaRho = 1/10;
    deltaTheta = 1;

    % calcul des valeurs maximum, respectivement pour q et p
    % (soit le nombre de pas delta maximum pour ces deux derniers)
    nbDeltaRho = round((rhoMax-rhoMin)/deltaRho);
    nbDeltaTheta = round((thetaMax-thetaMin)/deltaTheta);

    % initialisation de la matrice accumulatrice de Hough avec :
    % - le pas max de rhô pour les lignes
    % - le pas max de theta pour les colonnes
    H = zeros(nbDeltaRho, nbDeltaTheta);

    % déroulement du parcours de l'image matricielle I
    for i = 1:nbL
        for j = 1:nbC
            % si le pixel de coordonnées (i,j) est différent de zéro,
            % alors...
            if(I(i,j) ~= 0)
                % déroulement du parcours de l'indice p
                for p = 1:nbDeltaTheta
                    % calcul de la valeur de theta
                    theta = (p*pi)/thetaMax;
                    % calcul de la valeur de rhô
                    %rho = i*cos(theta)+j*sin(theta); (non fonctionnel ici)
                    rho = (j-1)*cos(theta)+(nbL-i)*sin(theta);
                    % calcul de la valeur arrondie de q
                    q = round((1/deltaRho)*(rho-rhoMin));
                    % affectation de la valeur suivante de la matrice de
                    % Hough dans la valeur courante de cette même matrice
                    H(q,p) = H(q,p)+1;
                end
            end
        end
    end

    %SEUILLAGE
    seuil=2.80; %J'AI FAIS A L'OEUIL (POUR L'IMAGE M1), FAUT VOIR COMMENT LE CALCULER
    H_seuil = H;

    for i=1:nbDeltaRho
        for j=1:nbDeltaTheta
            if H(i,j) < seuil
                H_seuil(i,j)=0;
            end
        end
    end
    figure;
    imagesc(H_seuil);

    %PRENDRE LES LIGNES (COUPLES DE RHO ET THETA)
    lignes = {}; %CELL ARRAY, COTIENT DES STRUCTURES
    cpt=1;
    for i=1:nbDeltaRho
        for j=1:nbDeltaTheta
            if H_seuil(i,j) ~=0
                %CRRER UNE LIGNE cpt AVEC DEUX VARIABLES a & b
                lignes.ligne(cpt).r = i*deltaRho; %rho
                lignes.ligne(cpt).t = j*deltaTheta; %theta
%                 lignes.ligne(cpt).r = i*nbDeltaRho; %rho
%                 lignes.ligne(cpt).t = j*nbDeltaTheta; %theta
                cpt=cpt+1;
            end
        end
    end

    %TRACER LES DROITES
    figure;
    axis([0,nbL,0,nbC]);
    for i=1:cpt-1
        a = cos(lignes.ligne(i).t);
        b = sin(lignes.ligne(i).t);
        x0=a*lignes.ligne(i).r;
        y0=b*lignes.ligne(i).r;
        x1 = uint8(x0 + 1000*(-b));
        y1 = uint8(y0 + 1000*(a));
        x2 = uint8(x0 - 1000*(-b));
        y2 = uint8(y0 - 1000*(a));

        %plot(x1,y1);
        plot([x1,x2],[y1,y2],'b');
    end

    % création d'une fenêtre
    f = figure;
    % ajout d'un titre principal sur cette fenêtre
    f.Name = "Détection de droites (avec l'image 'image_droites.png')";
    % redimmension de cette même fenêtre
    f.Position = [0, 100, 1400, 600];

    % sélection du premier cadran de la fenêtre
    subplot(2, 2, 1);
    % affichage de l'image I
    imshow(I);
    % ajout d'un titre
    title("Image source");

    % sélection du deuxième cadran de la fenêtre
    subplot(2, 2, 2);
    % affichage des résultats de la transformée de Hough classique
%     surf(H) VUE 3D
%     hold on
    imagesc(H)
    colormap(gray);
    % ajout d'un titre
    title("Courbes dans l'espace de Hough (affichage non optimal)");
    % ajout des titres pour les axes en abscisses et en ordonnées
    xlabel("θ (theta en degré °)");
    ylabel("ϱ (rhô en unité pixel)");

    % sélection du troisième cadran de la fenêtre
    subplot(2, 2, 3);
    % affichage des droites détectées
    %plot(rho);
    % ajout d'un titre
    title("Représentation 'normale' des droites dans le plan image");
    % ajout des titres pour les axes en abscisses et en ordonnées
    xlabel("x");
    ylabel("y");

    % sélection du quatrième cadran de la fenêtre
    subplot(2, 2, 4);
    % affichage des résultats de la transformée de Hough classique
    imagesc(H);
    colormap(gray);
    % ajout d'un titre
    title("Représentation des courbes dans l'espace de Hough");
    % redimension des axes en abscisses et en ordonnées
    axis([0 180 0 140]);  % OU... axis([0 thetaMax 0 rhoMax*20]);
    % inversion du sens de l'axe des ordonnées
    axis('xy');   %axis('ij'); pour remettre dans le sens initial
    set(gca,'XAxisLocation','top');
    % ajout des titres pour les axes en abscisses et en ordonnées
    xlabel("θp (theta en degré °)");
    ylabel("ϱq (rhô en unité pixel)");

    % calcul des valeurs de NG minimum et maximum de la matrice H
    valMin = min(min(H));
    valMax = max(max(H));
    % affichage de ces deux valeurs dans le terminal de Matlab
    disp("Valeur de NG minimum pour la matrice accumulatrice");
    valMin
    disp("Valeur de NG maximum pour la matrice accumulatrice");
    valMax

    % affectation des valeurs de la matrice H dans la matrice résultante
    res = H;
end
