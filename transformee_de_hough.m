function []=transformee_de_hough(img, seuil)
   % I = imread('images/image5.png');
    %% definition de la taille de l'image
    [nbL,nbC,p]= size(img);

    %transformation en image en niveau de gris
     if(p>1)
         I=rgb2gray(img);
     else
         I=img;
     end

    nbDroite = 50;%% dans la matrice des parametres, on va prendre les nbLignes premieres valeurs par ordre decroissant
    
    if(max(max(I))==1)
        Ib=I;
    else
        [BW,s] = edge(I,'Sobel');

        %%binarisation de l'image
        Ib = edge(I,'Canny',s);
        %imshow(Ib);
    end

    thetaMin=-90;
    thetaMax=89;
    thetaMatrix = thetaMin:thetaMax;

    % taille de l'image en diagonale pour estimer la longueur maximale diag'une droite en diagonale
    diag = sqrt(nbL^2 + nbC^2);

    % Define step size for rhoMatrix matrix
    deltaRho = 1;
    rho = -diag:deltaRho:diag;
    % Rescale rho
    rhoMatrix = 0:deltaRho:ceil(2*diag);

    % initialisation de la matrice accumulatrice
    H = zeros(length(rhoMatrix),length(thetaMatrix));

    % déroulement du parcours de l'image matricielle I
    for i = 1:nbL
        for j = 1:nbC
            if(Ib(i,j))
                for k = 1:length(thetaMatrix)
                    rho_tmp = j*cos(thetaMatrix(k)* pi/180) + i*sin(thetaMatrix(k) * pi/180);
                    rho_tmp = round((rho_tmp + diag)/deltaRho)+1;
                    H(rho_tmp,k) = H(rho_tmp,k) + 1;
                end
            end
        end
    end

        HCopy = H;
        %initialisation de la matrice des droites
        %seuil=0.2;

        res = zeros(nbDroite,2);
        numP = 0;
        threshold = seuil*max(H(:));
       while(numP<nbDroite)
           if(threshold<=max(HCopy(:)))
               numP = numP + 1;
               [rows,cols] = find(HCopy == max(HCopy(:)));
               res(numP,:) = [rows(1),cols(1)];
               HCopy(rows(1),cols(1)) = 0;
           else
               break;
           end
       end
       res = res(1:numP,:);

        figure()
        subplot(3,1,1);
        imshow(img);
        title("Image originale");
        hold off;
        subplot(3,1,2);
        imshow(I);
        title('image avec les droites detectées');
        hold on;

       %on dessine ensuite les lignes par dessus l'image originale
       %on tranforme d'abord les coordonnées polaire en coordonnées cartesiens

     for i = 1:size(res,1)
        rhoTemp = rho(res(i,1));
        theTemp = thetaMatrix(res(i,2));
        if theTemp == 0
            x1 = rhoTemp;
            x2 = rhoTemp;
            y1 = 1;
            y2 = size(I,1);
        else
            x1 = 1;
            x2 = size(I,2);
            y1 = (rhoTemp - x1*cos(theTemp*pi/180)) / sin(theTemp*pi/180);
            y2 = (rhoTemp - x2*cos(theTemp*pi/180)) / sin(theTemp*pi/180);
        end
        plot([x1,x2],[y1,y2],'r','LineWidth',2);
        
      end

      hold off;
      subplot(3,1,3);
      imagesc(H);
end
